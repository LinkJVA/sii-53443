##[2.0] - 28-10-2020
### Añadido
- Movimiento a la esfera y a las raquetas
- Instrucciones en README
- La Esfera ahora disminuye de tamaño con el tiempo

## [1.3] - 15-10-2020
### Añadido
- Etiqueta de fin de la práctica 1

## [1.1] - 15-10-2020
### Añadido
- Cabeceras en todos los archivos .cpp y .h indicando el autor del código
  mediante etiquetas.
